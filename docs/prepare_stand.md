# Prepare the microscope stand

{{BOM}}

[M3 nut]: parts/mechanical.yml#Nut_M3_SS

## Embed mounting nuts in the stage {pagestep}

* Take the [microscope stand][Microscope stand](fromstep){qty:1, cat:printedpart}
* Place an [M3 nut]{qty:4, cat:mech} in the slot under a mounting lug
* Put an [M3x8 cap head screw][extra M3x8 cap screw](parts/mechanical.yml#CapScrew_M3x8mm_SS){qty: 1, cat:tool} into the hole above the nut
* Tighten with a [2.5mm Allen key]{qty:1, cat:tool} until you feel reasonable resistance
* Unscrew and remove the screw. The nut should stay mounted.
* Repeat for the other three mounting lugs


The [prepared microscope stand]{output, qty:1} is now ready for assembly.

