#High-resolution microscope


**TODO: Add blurb about microscope**

Before you start building the microscope you will need to source all the components listed our [bill of materials]{bom}.


The assembly is broken up into several steps:

1. [.](test_your_printer.md){step}
1. [.](high_res_printing.md){step}
1. [.](prepare_main_body.md){step}
1. [.](prepare_stand.md){step}
1. [.](actuator_assembly.md){step}
1. [.](high_res_optics_module.md){step}
1. [.](mount_optics_and_microscope.md){step}
1. [.](illumination.md){step}
1. [.](motors.md){step}
1. [.](attach_clips.md){step}
1. [.](wiring.md){step}


**TODO: add reflection option**



