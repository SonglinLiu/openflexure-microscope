# Complete the wiring


**TODO: This page is a placeholder it needs to be completed**


{{BOM}}


Wire the motors into a [Sangaboard](parts/electronics/sangaboard.md){qty:1, cat:electronic}

Wire up the [Raspberry Pi](parts/electronics.yml#RaspberryPi){qty:1, cat:electronic} (you will need a [power supply][Raspberry Pi Power Supply](parts/electronics.yml#RaspberryPi_PowerSupply){qty:1, cat:electronic}).