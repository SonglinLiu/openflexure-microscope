# Mount the optics and the microscope


{{BOM}}


## Mount the optics {pagestep}

* Get the [2.5mm Allen key]{qty:1, cat:tool, note: "- Must be a ball-ended key"} ready
* Take the [compete optics module](fromstep){qty:1, cat:subassembly} and pass it through the bottom of the [microscope][microscope with assembled actuators](fromstep){qty:1, cat:subassembly}.
* Insert exposed the mounting screw on the optics module through the keyhole on the z-actuator.
* Insert the Allen key through the teardrop shaped hole on the front of the microscope. Until it engages with the mounting screw.
* Slide optics module up the keyhole as high as it will go while still keeping the Allen key engaged with the screw. **Note: this is not the top of the keyhole**
* Tighten the screw with the Allen key to lock the optics in place


## Mount the microscope {pagestep}

* Place the microscope onto the [microscope stand][prepared microscope stand](fromstep){qty:1, cat:subassembly}.
* The lugs on the microscope should sit on the lugs of the stand.
* Use four [M3x8 cap head screws](parts/mechanical.yml#CapScrew_M3x8mm_SS){qty: 4, cat:mech} to fix the microscope in place using the same [Allen key][2.5mm Allen key]